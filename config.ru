require 'rack'
require_relative 'lib/omadacontroller/exporter'
require_relative 'lib/sg108pe/exporter'

use Rack::Deflater
use OmadaController::Exporter, { hostname: '192.168.178.220', path: '/omada/metrics', username: 'user', password: 'pass' }
use Sg108Pe::Exporter, { hostname: '192.168.178.41', path: '/sg108pe/metrics', username: 'user', password: 'pass' }

run ->(_) { [200, { 'content-type' => 'text/html' }, ['OK']] }
