require 'net/http'
require "prometheus/client"
require "prometheus/client/formats/text"
require 'json'
require 'logger'

module OmadaController
  class Exporter
    $logger = Logger.new(STDOUT, datetime_format: '%Y-%m-%d %H:%M:%S')

    def initialize(app, options = {})
      @app = app
      @path = options[:path] || '/metrics'
      @prometheus = options[:registry] || Prometheus::Client::Registry.new
      @metrics = register

      @hostname = options[:hostname]
      @username = options[:username]
      @password = options[:password]

      @http = Net::HTTP.new(@hostname, 443)
      @http.use_ssl = true
      @http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      @http.start

      @format = Prometheus::Client::Formats::Text
      @expiry = Time.now
      @token = nil
    end

    def call(env)
      if @path == env["PATH_INFO"]
        update_ssid_metrics
        update_ap_metrics
        update_client_metrics
        [200, { 'Content-Type' => 'text/plain' }, [@format.marshal(@prometheus)]]
      else
        @app.call(env)
      end
    end

    def login
      uri = URI("https://#{@hostname}/api/user/login?ajax")
      req = Net::HTTP::Post.new uri
      req.body = { 'method': 'login', 'params': { 'name': @username, 'password': @password } }.to_json
      req["Content-Type"] = "application/json; charset=UTF-8"

      response = @http.request req
      response_body = JSON.parse response.body

      @token = response_body["result"]["token"]
      @expiry = Time.now + 45 * 60

      cookies = response.get_fields("set-cookie")
      session_id = cookies.find { |c| c.start_with? "TPEAP_SESSIONID" }
      match = session_id.match /^TPEAP_SESSIONID=([[:xdigit:]]{8}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{12})/
      @session_id = match[1]

      $logger.debug "Token refreshed"
    end

    def ssid_stats_store
      login if Time.now > @expiry
      uri = URI("https://#{@hostname}/web/v1/controller?ssidStatsStore&token=#{@token}")
      req = Net::HTTP::Post.new uri
      req.body = '{"method":"getSsidStats","params":{}}'
      req["Content-Type"] = "application/json; charset=UTF-8"
      req["Cookie"] = "TPEAP_SESSIONID=#{@session_id}; token=#{@token}; userLevel=3"

      response = @http.request req
      JSON.parse response.body
    end

    def current_usage_store
      login if Time.now > @expiry
      uri = URI("https://#{@hostname}/web/v1/controller?currentUsageStore&token=#{@token}")
      req = Net::HTTP::Post.new uri
      req.body = '{"method":"getTopUsage","params":{"sortOrder":"asc","currentPage":1,"currentPageSize":5}}'
      req["Content-Type"] = "application/json; charset=UTF-8"
      req["Cookie"] = "TPEAP_SESSIONID=#{@session_id}; token=#{@token}; userLevel=3"

      response = @http.request req
      JSON.parse response.body
    end

    def user_store
      login if Time.now > @expiry
      uri = URI("https://#{@hostname}/web/v1/controller?userStore&token=#{@token}")
      req = Net::HTTP::Post.new uri
      req.body = '{"method":"getGridActiveClients","params":{"sortOrder":"desc","currentPage":1,"currentPageSize":100,"sortName":"ssid","filters":{"type":"all"}}}'
      req["Content-Type"] = "application/json; charset=UTF-8"
      req["Cookie"] = "TPEAP_SESSIONID=#{@session_id}; token=#{@token}; userLevel=3"

      response = @http.request req
      JSON.parse response.body
    end

    def register
      ap_labels = [:ap_mac, :ap_name]
      client_labels = [:name, :mac]

      {
        :total_download => @prometheus.counter(:ssid_total_download_bytes_total,
                                               docstring: 'The total number of bytes downloaded',
                                               labels: [:ssid]),
        :total_upload => @prometheus.counter(:ssid_total_upload_bytes_total,
                                             docstring: 'The total number of bytes uploaded',
                                             labels: [:ssid]),
        :total_client => @prometheus.gauge(:ssid_total_clients_total,
                                           docstring: 'The total number of clients connected',
                                           labels: [:ssid]),
        :total_traffic => @prometheus.counter(:ssid_total_traffic_bytes_total,
                                              docstring: 'The total traffic in bytes',
                                              labels: [:ssid]),
        :ap_download => @prometheus.counter(:access_point_download_bytes,
                                            docstring: 'The number of bytes downloaded by an AP',
                                            labels: ap_labels),
        :ap_upload => @prometheus.counter(:access_point_upload_bytes,
                                          docstring: 'The number of bytes uploaded by an AP',
                                          labels: ap_labels),
        :ap_traffic => @prometheus.counter(:access_point_traffic_bytes,
                                           docstring: 'The total traffic in bytes by an AP',
                                           labels: ap_labels),
        :ap_client => @prometheus.gauge(:access_point_client_count,
                                        docstring: 'The number of clients connected to an AP',
                                        labels: ap_labels),
        :ap_boot_time => @prometheus.gauge(:access_point_last_boot_time,
                                           docstring: 'The last boot time of an AP',
                                           labels: ap_labels),
        :ap_cpu_usage => @prometheus.gauge(:access_point_cpu_usage_ratio,
                                           docstring: 'The CPU usage of an AP',
                                           labels: ap_labels),
        :ap_memory_usage => @prometheus.gauge(:access_point_memory_usage_ratio,
                                              docstring: 'The memory usage of an AP',
                                              labels: ap_labels),

        :client_rate_mbps => @prometheus.gauge(
          :client_rate_mbps,
          docstring: 'The rate that is negotiated between client and access point (Mbps)',
          labels: client_labels),
        :client_signal_to_noise_ratio => @prometheus.gauge(
          :client_signal_to_noise_ratio,
          docstring: 'The signal-to-noise ratio',
          labels: client_labels),
        :client_rssi => @prometheus.gauge(
          :client_rssi,
          docstring: 'The RSSI value',
          labels: client_labels),
        :client_download_bytes => @prometheus.counter(
          :client_download_bytes,
          docstring: 'The number of bytes downloaded by the client',
          labels: client_labels),
        :client_upload_bytes => @prometheus.counter(
          :client_upload_bytes,
          docstring: 'The number of bytes uploaded by the client',
          labels: client_labels),
        :client_first_seen_time => @prometheus.gauge(
          :client_first_seen_time,
          docstring: 'The first time the client was seen',
          labels: client_labels),
        :client_last_seen_time => @prometheus.gauge(
          :client_last_seen_time,
          docstring: 'The last time the client was seen',
          labels: client_labels),
      }
    end

    def update_ssid_metrics
      data = self.ssid_stats_store
      data['result']['ssidList'].each do |item|
        ssid = item['ssid']
        @metrics[:total_download].increment(
          by: [0, item['totalDownload'] - @metrics[:total_download].get(labels: { ssid: ssid })].max,
          labels: { ssid: ssid })
        @metrics[:total_upload].increment(
          by: [0, item['totalUpload'] - @metrics[:total_upload].get(labels: { ssid: ssid })].max,
          labels: { ssid: ssid })
        @metrics[:total_traffic].increment(
          by: [0, item['totalTraffic'] - @metrics[:total_traffic].get(labels: { ssid: ssid })].max,
          labels: { ssid: ssid })
        @metrics[:total_client].set(item['totalClient'], labels: { ssid: ssid })
      end
    end

    def update_ap_metrics
      now = Time.now.to_i
      current_usage = self.current_usage_store
      current_usage['result']['data'].each do |item|
        labels = { ap_mac: item['apMac'], ap_name: item['apName'] }
        @metrics[:ap_traffic].increment(
          by: [0, item['totalTraffic'] - @metrics[:ap_traffic].get(labels: labels)].max,
          labels: labels)
        @metrics[:ap_upload].increment(
          by: [0, item['upload'] - @metrics[:ap_upload].get(labels: labels)].max,
          labels: labels)
        @metrics[:ap_download].increment(
          by: [0, item['download'] - @metrics[:ap_download].get(labels: labels)].max,
          labels: labels)
        @metrics[:ap_client].set(item['clientNum'], labels: labels)
        boot_time = (now - item['uptime']) / 60 * 60 # uptime is not very consistent
        @metrics[:ap_boot_time].set(boot_time, labels: labels)
        @metrics[:ap_cpu_usage].set(item['cpuRate'] / 100.0, labels: labels)
        @metrics[:ap_memory_usage].set(item['memoryRate'] / 100.0, labels: labels)
      end
    end

    def update_client_metrics
      users = self.user_store
      users['result']['data'].each do |item|
        labels = { name: item['name'], mac: item['mac'] }

        @metrics[:client_rate_mbps].set(
          item['rate'],
          labels: labels)
        @metrics[:client_signal_to_noise_ratio].set(
          item['snr'],
          labels: labels)
        @metrics[:client_rssi].set(
          item['rssi'],
          labels: labels) if item['rssi']
        @metrics[:client_first_seen_time].set(
          item['firstSeen'],
          labels: labels)
        @metrics[:client_last_seen_time].set(
          item['lastSeen'],
          labels: labels)

        @metrics[:client_download_bytes].increment(
          by: [0, item['download'] - @metrics[:client_download_bytes].get(labels: labels)].max,
          labels: labels)
        @metrics[:client_upload_bytes].increment(
          by: [0, item['upload'] - @metrics[:client_upload_bytes].get(labels: labels)].max,
          labels: labels)

      end
    end
  end
end