require 'rack'

module Sg108Pe
  class Exporter
    $logger = Logger.new(STDOUT, datetime_format: '%Y-%m-%d %H:%M:%S')

    def initialize(app, options = {})
      @app = app
      @path = options[:path] || '/metrics'
      @prometheus = options[:registry] || Prometheus::Client::Registry.new
      @metrics = register
      @expiry = Time.now

      @hostname = options[:hostname]

      @http = Net::HTTP.new(@hostname, 80)

      @format = Prometheus::Client::Formats::Text
    end

    def call(env)
      if @path == env["PATH_INFO"]
        update_metrics
        [200, { 'Content-Type' => 'text/plain' }, [@format.marshal(@prometheus)]]
      else
        @app.call(env)
      end
    end

    def register
      {
        :tx_good => @prometheus.counter(:switch_tx_good_packets, docstring: 'The number of good packets transmitted', labels: [:port]),
        :tx_bad => @prometheus.counter(:switch_tx_bad_packets, docstring: 'The number of bad packets transmitted', labels: [:port]),
        :rx_good => @prometheus.counter(:switch_rx_good_packets, docstring: 'The number of good packets received', labels: [:port]),
        :rx_bad => @prometheus.counter(:switch_rx_bad_packets, docstring: 'The number of bad packets received', labels: [:port]),
        :link_status => @prometheus.gauge(:switch_link_status, docstring: 'The status of the link (0 = Link Down, 5 = 100 Full Duplex, 6 = 1000 Full Duplex)', labels: [:port]),
      }
    end

    def logon
      uri = URI("http://#{@hostname}/logon.cgi")
      req = Net::HTTP::Post.new uri

      req.set_form_data({"username" => "sg108pe", "password" => "76Yx8ZjRz8P6xNK", "logon" => "Login"})

      response = @http.request(req)
      @expiry = Time.now + 45 * 60
    end

    def update_metrics
      logon # if Time.now > @expiry
      uri = URI("http://#{@hostname}/PortStatisticsRpm.htm")
      req = Net::HTTP::Get.new uri

      response = @http.request req
      body = response.body

      m = /pkts:(\[[0-9,]+\])/.match(body)
      data = JSON.parse(m[1])

      8.times do |port|
        portstr = (port + 1).to_s
        @metrics[:tx_good].increment(by: data[4 * port] - @metrics[:tx_good].get(labels: { port: portstr }), labels: { port: portstr })
        @metrics[:tx_bad].increment(by: data[4 * port + 1] - @metrics[:tx_bad].get(labels: { port: portstr }), labels: { port: portstr })
        @metrics[:rx_good].increment(by: data[4 * port + 2] - @metrics[:rx_good].get(labels: { port: portstr }), labels: { port: portstr })
        @metrics[:rx_bad].increment(by: data[4 * port + 3] - @metrics[:rx_bad].get(labels: { port: portstr }), labels: { port: portstr })
      end

      m = /link_status:(\[[0-9,]+\])/.match(body)
      data = JSON.parse(m[1])

      8.times do |port|
        portstr = (port + 1).to_s
        @metrics[:link_status].set(data[port], labels: { port: portstr })
      end
    end
  end
end
